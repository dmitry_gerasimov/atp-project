<?php
error_reporting(E_ALL);
include ("config.php");

spl_autoload_register(function ($className)
{
    $className = strtolower($className);
    $file = SITE_PATH . $className . '.php';
    if(file_exists($file))
        require_once(SITE_PATH . $className . '.php');
});

try{
    Core\Router::buildRoute();
    Core\Router::start();
}catch(\Model\Obj\Exception $ex)
{
    echo $ex->what();
}
?>