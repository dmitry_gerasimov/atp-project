<!DOCTYPE html>
<html>
<head>
    <meta charset='UTF-8' />
    <title><?php echo $data['title'] ?></title>
</head>
<body>
    <?php
     if($content != null) 
        include(SITE_PATH . 'view\templates\\' . $content); 
    ?>
</body>
</html>