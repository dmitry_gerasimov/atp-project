<?php


namespace View;

class Cars
{

    public function __construct()
    {
        
    }

    public function generate($template, $content, $data)
    {
        include SITE_PATH . 'view\\templates\\' . $template;
    }
}