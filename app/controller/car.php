<?php


namespace Controller;

class Car extends Base
{
    public function __construct(array $args, int $argc)
    {
        Base::__construct($args, $argc);
        $this->model = new \Model\ModelCars(TABLE_CARS, 'Model\\Obj\\Car');
        $this->view = new \View\Cars();
    }

    public function list()
    {
        $data['cars'] = $this->model->getAll();
        $data['title'] = 'Список машин';
        $this->view->generate('main.php', 'cars' . DS . 'cars.php', $data);
    }

    public function detail()
    {
        if($this->argc != 1)
            throw new \Model\Obj\Exception(31, 'Invalid count of parameters');
        $id = (int)$this->args[0];
        $data['car'] = $this->model->getOne($id);
        $data['title'] = $data['car']->getCarName();
        $this->view->generate('main.php', 'cars' . DS . 'detail.php', $data);
    }

    public function edit()
    {
        if($this->argc != 1)
            throw new \Model\Obj\Exception(31, 'Invalid count of parameters');
        $id = (int)$this->args[0];
        $data['car'] = $this->model->getOne($id);
        $data['title'] = $data['car']->getCarName();
        $this->view->generate('main.php', 'cars' . DS . 'edit.php', $data);
    }

    public function editcar()
    {
        try
        {
            $car = new \Model\Obj\Car($_POST);
        }catch(\Model\Obj\Exception $ex)
        {
            throw new \Model\Obj\Exception(61, 'Полученные данные неполные или неверные');
        }
        $id = (int)$_POST['id'];
        return $this->model->update($id, $car);
    }

    public function newcar()
    {
        $data['title'] = 'Новая машина';
        $this->view->generate('main.php', 'cars' . DS . 'new_car.php', null);
    }

    public function addcar()
    {
        $data['title'] = 'Машина добавлена';
        try
        {
            $car = new \Model\Obj\Car($_POST);
        }catch(\Model\Obj\Exception $ex)
        {
            throw new \Model\Obj\Exception(61, 'Полученные данные неполные или неверные');
        }
        $this->model->add($car);
        $this->view->generate('main.php', null, null);
    }

}