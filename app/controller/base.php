<?php

namespace Controller;

Abstract class Base
{
    protected $model;
    protected $view;
    protected $args;
    protected $argc;

    public function __construct(array $args, int $argc)
    {
        $this->args = $args;
        $this->argc = $argc;
    }    
}