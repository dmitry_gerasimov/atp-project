<?php

define ('DS', DIRECTORY_SEPARATOR);
$sitePath = realpath(dirname(__FILE__)) . DS;
define ('SITE_PATH', $sitePath);

//tables
define('TABLE_DRIVERS', 'drivers');
define('TABLE_CARS', 'cars');
define('TABLE_WAYBILLS', 'waybills');
define('TABLE_USERS', 'users');

//connection
define('SERVER', 'localhost');
define('USER', 'atp_user');
define('PASSWORD', 'Gsu96AsDVtU1Mgmm');
define('DATABASE', 'atp');

//base
define('BASE_CONTROLLER', 'base');
define('BASE_ACTION', '__standart');
define('BASE_MODEL', 'base');

define('CONTROLLERS', 'controller' . DS);
define('MODELS', 'model' . DS . 'obj' . DS);
define('VIEWS', 'view' . DS);
