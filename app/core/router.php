<?php

namespace Core;

class Router
{
    private static $controller;
    private static $class;
    private static $action;
    private static $args;

   public static function buildRoute()
   {
        self::$controller = BASE_CONTROLLER;
        self::$action = BASE_ACTION;
        $model = BASE_MODEL;

        $path = trim($_SERVER['REQUEST_URI'], '/');
        $parts = explode('/', $path);
        array_shift($parts); //gets '/app/' from URI
        $path = SITE_PATH . CONTROLLERS;
        if(!empty($parts))
        {
            $part = array_shift($parts);
            self::$class = '\\Controller\\' . ucfirst(strtolower($part));
            self::$controller = strtolower($part) . '.php';
        }
        if(!is_file($path . self::$controller))
            throw new \Model\Obj\Exception(11, 'Unable to get controller ' . self::$controller . '.');
        if(!empty($parts))
            self::$action = strtolower(array_shift($parts));
        self::$args = $parts;
   }

   public static function start()
   {
        $class = new self::$class(self::$args, count(self::$args));
        $method = self::$action;
        if(!is_callable(array($class, $method)))
            throw new \Model\Obj\Exception(12, 'Action \'' . $method . '\' is not a callable function');
        $class->$method();
   }
}