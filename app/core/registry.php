<?php

namespace Core;

class Registry
{
    private $vars;

    public function __construct()
    {
        $this->vars = array();
    }

    public function set($key, $value)
    {
        if(isset($this->vars[$key]))
            throw new Model\Obj\Exception(11, 'Unable to set key = ' . $key . ' is already exists');
        $this->vars[$key] = $value;
        return true;
    }

    public function isset($key) : bool
    {
        return isset($this->vars[$key]);
    }

    public function get($key)
    {
        if(!isset($this->vars[$key]))
            return null;
        return $this->vars[$key];
    }

    public function remove($key)
    {
        unset($this->vars[$key]);
        return true;
    }
}