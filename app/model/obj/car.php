<?php

namespace Model\Obj;

class Car implements iBase
{
    private  $id;
    private  $carName;
    private  $c10;
    private  $c11;
    private  $c12;
    private  $c16;

   /* public function __construct(string $carName, float $price, float $c11, float $c12, float $c16)
    {
        $this->setCarName($carName);
        $this->setPrice($price);
        $this->setC11($c11);
        $this->setC12($c12);
        $this->setC16($c16);
    }*/

    public function __construct(array $result)
    {
        if(isset($result['id']))
            $this->setId($result['id']);
        else
            $this->setId(0);
        $this->setCarName($result['carName']);
        $this->setPrice($result['price']);
        $this->setC11($result['c11']);
        $this->setC12($result['c12']);
        $this->setC16($result['c16']);
    }

    public function getId() : int
    {
        return $this->id;
    }

    private function setId($id)
    {
        $this->id = $id;
    }

    public function setCarName(string $carName)
    {
        if($carName == null)
            throw new Exception(51, 'null pointer');
        if(empty($carName))
            throw new Exception(52, 'empty string');
        $this->carName = $carName;
    }

    public function setPrice(float $price)
    {
        if($price == null)
            throw new Exception(51, 'null pointer');
        $this->c10 = $price;
    }

    public function setC11(float $c11)
    {
        if($c11 == null)
            throw new Exception(51, 'null pointer');
        $this->c11 = $c11;
    }

    public function setC12(float $c12)
    {
        if($c12 == null)
            throw new Exception(51, 'null pointer');
        $this->c12 = $c12;
    }

    public function setC16(float $c16)
    {
        if($c16 == null)
            throw new Exception(51, 'null pointer');
        $this->c16 = $c16;
    }

    public function getCarName(): string 
    {
        return $this->carName;
    }

    public function getPrice(): float 
    {
        return $this->c10;
    }

    public function getC11(): float 
    {
        return $this->c11;
    }

    public function getC12(): float
    {
        return $this->c12;
    }

    public function getC16(): float
    {
        return $this->c16;
    }

    public function __toString(): string
    {
        return 'Car [carName: ' . $this->getCarName() . ', price: ' . $this->getPrice() . ', coefficients: [1.1: ' . $this->getC11() . ', 1.2: ' . $this->getC12() . ', 1.6: ' . $this->getC16() . ']]';
    }

    public function toArray(): array
    {
        return ['carName' => $this->getCarName(), 'price' => $this->getPrice(), 'c11' => $this->getC11() ,'c12' => $this->getC12(), 'c16' => $this->getC16()];
    }

    public function getFields(): array
    {
        return ['id', 'carName', 'price', 'c11', 'c12', 'c16'];
    }
}