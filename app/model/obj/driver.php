<?php

namespace Model\Obj;

class Driver implements iBase
{
    private  $id;
    private  $lastName;
    private  $middleName;
    private  $firstName;

    public function __construct(array $result)
    {
        if(isset($result['id']))
            $this->setId($result['id']);
        else
            $this->setId(0);
        $this->setFirstName($result['firstName']);
        $this->setMiddleName($result['middleName']);
        $this->setLastName($result['lastName']);
    }

    private function setId(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setFirstName(string $firstName)
    {
        if($firstName == null)
            throw new Exception(51, 'null pointer');
        if(empty($firstName))
            throw new Exceltion(52, 'empty string');
        $this->firstName = $firstName;
    }

    public function setMiddleName(string $middleName)
    {
        if($middleName == null)
            throw new Exception(51, 'null pointer');
        if(empty($middleName))
            throw new Exceltion(52, 'empty string');
        $this->middleName = $middleName;
    }

    public function setLastName(string $lastName)
    {
        if($lastName == null)
            throw new Exception(51, 'null pointer');
        if(empty($lastName))
            throw new Exceltion(52, 'empty string');
        $this->lastName = $lastName;   
    }

    public function getFirstName() : string
    {
        return $this->firstName;
    }

    public function getMiddleName() : string
    {
        return $this->middleName;
    }

    public function getLastName() : string
    {
        return $this->lastName;
    }
    public function __toString(): string
    {
        return '[' . $this->getFirstName() . ' ' . $this->getMiddleName() . ' ' . $this->getLastName() .']';
    }

    public function toArray(): array
    {
        return ['firstName' => $this->getFirstName(), 'middleName' => $this->getMiddleName(), 'lastName' => $this->getLastName()];
    }

    public function getFields(): array
    {
        return ['id', 'firstName', 'middleName', 'lastName'];
    }
}
