<?php


namespace Model\Obj;

interface iBase
{
    public function __toString(): string;
    public function toArray(): array;    
    public function getFields(): array;
}