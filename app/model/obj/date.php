<?php

namespace Model\Obj;

class Date implements iBase
{
    private  $day;
    private  $month;
    private  $year;

    public function __construct(string $date)
    {
        $this->setDate($date);
    }

    public function setDate(string $date) 
    {
        if($date == null)
            throw new Exception(51, 'null pointer');
        if(empty($date))
            throw new Exceltion(52, 'empty string');
        $vars = explode(".", $date);
        if(count($vars) != 3)
            throw new Exception(1, "Не удалось обработать дату!");
        if(!checkdate($vars[1], $vars[0], $vars[2]))
            throw new Exception(2, "Неверно введена дата!");
        $this->day = $vars[0];
        $this->month = $vars[1];
        $this->year = $vars[2];
    }

    public function getDate(): string
    {
        return $this->getYear() . '-' . $this->getMonth() . '-' . $this->getDay();
    }

    public function getDay(): int
    {
        return $this->day;
    }

    public function getMonth(): int
    {
        return $this->month;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function __toString(): string
    {
        return '[' . $this->getDay() . '. ' . $this->getMonth() . '. ' . $this->getYear() . '] ';
    }

    public function toArray(): array
    {
        return ['date' => $this->getDate(), 'day' => $this->getDay(), 'month' => $this->getMonth(),
         'year' => $this->getYear()];
    }

    public function getFields(): array
    {
        return ['day', 'month', 'year', 'date'];
    }
}