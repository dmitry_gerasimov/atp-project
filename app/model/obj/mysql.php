<?php

namespace Model\Obj;

class MySQL implements iDAO
{
    private $db;

    public function __construct($server, $user, $password, $database)
    {
        $opt = array(
            'host' => $server,
            'user' => $user,
            'pass' => $password,
            'db' => $database,
            'exception' => 'Exception'
        );
        $this->db = new Libs\SafeMySQL($opt);
    }

    public function getDrivers() : array
    {
        return $this->db->getAll("SELECT * FROM `drivers`");
    }
    public function getCars(): array
    {
        return $this->db->getAll("SELECT * FROM `cars`");
    }
    public function getDrivers(string $condition): array
    {
        return $this->db->getAll('SELECT * FROM `drivers` WHERE ' . $condition);
    }
    public function getCars(string $condition): array
    {
        return $this->db->getAll('SELECT * FROM `cars` WHERE ' . $condition);
    }
    public function getUsers(): array
    {
        return $this->db->getAll('SELECT * FROM `users`');
    }
    public function getUsers(string $condition): array
    {
        return $this->db->getAll('SELECT * FROM `users` WHERE ' . $condition);
    }
}