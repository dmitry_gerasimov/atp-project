<?php

namespace Model\Obj;

class Exception extends \Exception
{
    private  $num;
    protected  $message;

    public function __construct(int $num, string $message = null)
    {
        $this->num = $num;
        $this->message = $message;
    }

    public function what() : string
    {
        return '[' . $this->num . '] ' . ($this->message == null ? ' ' : $this->message);
    }
}