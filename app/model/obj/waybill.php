<?php

namespace Model\Obj;

class Waybill implements iBase
{
    private  $id;
    private  $driver;
    private  $car;
    private  $date;
    private  $time;

    public function __construct(Driver $driver, Car $car, Date $date, Time $time)
    {
        $this->setId(0);
        $this->setDriver($driver);
        $this->setCar($car);
        $this->setDate($date);
        $this->setTime($time);
    }

    private function setId(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setDriver(Driver $driver)
    {
        if($driver == null)
            throw new Exception(0, "Нулевой указатель на объект Driver");
        $this->driver = $driver;
    }

    public function setCar(Car $car)
    {
        if($car == null)
            throw new Exception(0, "Нулевой указатель на объект Car");
        $this->car = $car;
    }

    public function setDate(Date $date)
    {
        if($date == null)
            throw new Exception(0, "Нулевой указатель на объект Date");
        $this->date = $date;
    }

    public function setTime(Time $time)
    {
        if($time == null)
            throw new Exception(0, "Нулевой указатель на объект Time");
        $this->time = $time;
    }

    public function getDriver(): Driver
    {
        return $this->driver;
    }

    public function getCar(): Car
    {
        return $this->car;
    }

    public function getDate(): Date
    {
        return $this->date;
    }

    public function getTime(): Time
    {
        return $this->time;
    }

    public function __toString(): string
    {
        return $this->getDriver()->toString() . ' '
                . $this->getCar()->toString() . ' '
                . $this->getDate()->toString() . ' '
                . $this->getTime()->toString();
    }

    public function toArray(): array
    {
        return ['driver' => $this->getDriver()->toArray(), 'car' => $this->getCar()->toArray(),
                'date' => $this->getDate()->toArray(), 'time' => $this->getTime()->toArray(),
                'driverId' => $this->getDriver()->getId(),
                'carId' => $this->getCar()->getId(), 'date' => $this->getDate()->getDate(),
                'timeStart' => $this->getTime()->getStartTime(), 'timeEnd' => $this->getTime()->getEndTime()
    ];
    }

    public function getFields(): array
    {
        return ['id', 'driverId', 'carId', 'date', 'timeStart', 'timeEnd'];
    }
}