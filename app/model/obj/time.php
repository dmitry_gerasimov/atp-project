<?php

namespace Model\Obj;

class Time implements iBase
{
    private  $start;
    private  $end;

    private function check_time($time): bool
    {
        return preg_match('/^([01]?[0-9]|2[0-3])(:)[0-5][0-9]$/', $time);
    }

    public function __construct(string $start, string $end)
    {
        if($start == null || $end == null)
            throw new Exception(0, 'Нулевой указатель на строку со временем');
        if(!$this->check_time($start) || !$this->check_time($end))
            throw new Exception(3, 'Неверно введено время! Вводите время в формате ЧЧ:ММ');
        $this->start = $start;
        $this->end = $end;
        $this->getDifference();
    }

    public function getStartTime(): string
    {
        return $this->start;
    }

    public function getEndTime(): string
    {
        return $this->end;
    }

    public function getDifference(): string
    {
        $startHours = (int)(explode(':', $this->start))[0];
        $endHours = (int)(explode(':', $this->end))[0];
        $diffHourse = $endHours - $startHours;

        $startMinutes = (int)(explode(':', $this->start))[1];
        $endMinutes = (int)(explode(':', $this->end))[1];
        $diffMinutes = $endMinutes - $startMinutes;
        if($diffMinutes < 0)
        {
            if(--$diffHourse < 0)
                throw new Exception(4, 'Неверно задан временной промежуток');
            $diffMinutes = 60 + $endMinutes - $startMinutes;
        }
        return $diffHourse . ':' . $diffMinutes;
    }

    public function getMinutes(): int
    {
        $startMinutes = (int)(explode(':', $this->start))[1];
        $endMinutes = (int)(explode(':', $this->end))[1];
        return $endMinutes - $startMinutes;
    }

    public function __toString(): string
    {
        return 'time: ' . $this->getStartTime() . ' - ' . $this->getEndTime();
    }

    public function toArray(): array
    {
        return ['timeStart' => $this->getStartTime(), 'timeEnd' => $this->getEndTime(),
         'difference' => $this->getDifference()];
    }

    public function getFields(): array
    {
        return ['timeStart', 'timeEnd'];
    }
}