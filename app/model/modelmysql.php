<?php

namespace Model;

class ModelMySQL implements Model
{
    protected static $db = null;
    protected $table;
    protected $className;

    protected $whitelist = [
        TABLE_DRIVERS, TABLE_CARS, TABLE_USERS, TABLE_WAYBILLS
    ];

    private function checkClassName(string $className)
    {
     /* 
     
     */

    return true;
    }

    public function __construct(string $table, string $className)
    {
        $opt = array(
            'host' => SERVER,
            'user' => USER,
            'pass' => PASSWORD,
            'db' => DATABASE
        );
        
        if(self::$db == null)
            self::$db = new \Libs\SafeMySQL($opt);
        $this->table = $table;
        if(!in_array($this->table, $this->whitelist, true))
            throw new Obj\Exception(105, 'Указанная таблица не существует');
        if(!$this->checkClassName($className))
            throw new Obj\Exception(666, 'Не удалось создать экземпляр переданного класса');
        $this->className = $className;
    }

    public function getAll() : array
    {
        $result = self::$db->getAll('SELECT * FROM ?n ', $this->table);
        $objts = array();
        foreach($result as $r)
            $objts[] = new $this->className($r);
        return $objts;
    }

    public function getOne(int $id) : Obj\iBase
    {
        $result = self::$db->getRow('SELECT * FROM ?n WHERE `id` = ?i', $this->table, $id);
        if(!$result)
            throw new Obj\Exception(101, 'Указанное значение не найдено.');
        return new $this->className($result);
    }

    public function add(Obj\iBase $object): Obj\iBase
    {
        $allowed = $object->getFields();
        $data = $object->toArray();
        $data = self::$db->filterArray($data, $allowed);
        $sql = 'INSERT INTO ?n SET ?u';
        $res = self::$db->query($sql, $this->table, $data);
        if(!$res)
            throw new Obj\Exception(55, 'Error with adding some object');
        return $object;
    }

    public function update(int $id, Obj\iBase $object): Obj\iBase
    {
        $allowed = $object->getFields();
        $data = $object->toArray();
        $data = self::$db->filterArray($data, $allowed);
        $sql = 'UPDATE ?n SET ?u WHERE `id` = ?i';
        $res = self::$db->query($sql, $this->table, $data, $id);
        if(!$res)
            throw new Obj\Exception(55, 'Error with updating some object');
        return $object;
    }

    public function delete(int $id): bool
    {
        self::$db->query('DELETE FROM ?n WHERE `id` = ?i', $this->table, $id);
        return true;
    }

   
}