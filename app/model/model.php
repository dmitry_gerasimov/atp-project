<?php


namespace Model;

interface Model
{
    public function __construct(string $table, string $className);
    public function getAll() : array;
    public function getOne(int $id): Obj\iBase;
    public function add(Obj\iBase $object): Obj\iBase;
    public function delete(int $id): bool;
    public function update(int $id, Obj\iBase $object): Obj\iBase;
}